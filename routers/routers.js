var express = require('express');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/bookshelf';
var assert = require('assert');
var router = express.Router();
var data = [];
var fs = require('fs');
var stylus = require('stylus');
var styles = fs.readFileSync(__dirname + '/../assets/stylus/style.styl', 'utf8');

router
  .get('/', function(req, res) {
    stylus(styles)
      .render(function(err, css){
        if (err) throw err;

      fs.writeFile(__dirname + '/../public/css/style.css', css, function(err, data) {
        if (err) throw err;
      });
    });

    MongoClient.connect(url, function(err, db) {
      var collection = db.collection('books');

      collection.find({}).toArray(function(err, docs) {
        data = docs;
        db.close();
      });
    });
    res.render('index', data);
  });

router
  .get('/addBook', function(req, res, next) {
    MongoClient.connect(url, function(err, db) {
      var collection = db.collection('books');

      collection.find({}).toArray(function(err, docs) {
        db.close();

        if (err) {
          console.log('Err with find - get - /addBook: ' + err);
          next(new Error(err));
          return;
        }

        data = docs;

        var item = data[data.length-1];
        if (item) {
          res.json(item);
        } else {
          res.json({error: "error"});
        }

      });
    });
});

router
  .get('/books', function(req, res) {
    MongoClient.connect(url, function(err, db) {
      var collection = db.collection('books');

      collection.find({}).toArray(function(err, docs) {
        if (err) {
          console.log('Err with find - get - /books: ' + err);
          res.json(err);
        } else {
          data = docs;
          res.json(data);
        }
        db.close();
      });
    });
  })
  .post('/books', function(req, res) {
    MongoClient.connect(url, function(err, db) {
      var collection = db.collection('books');
      var doc = req.body; // TODO validate

      collection.insert(doc, function(err, result) {
        if (err) {
            console.log('Err with insert - post - /books: ' + err);
            res.json(err);
        } else {
            res.json(result.ops[0]);
        }
        db.close();
      });
    });

    MongoClient.connect(url, function(err, db) {
      var collection = db.collection('books');

      collection.find({}).toArray(function(err, docs) {
        if (err) {
            console.log('Err with find - post - /books: ' + err);
            res.json(err);
        } else {
          data = docs;
          res.json(data[data.length]);
        }
        db.close();
      });
    });

  });

router
  .post('/delete', function(req, res) {
    MongoClient.connect(url, function(err, db) {
      var collection = db.collection('books');

      collection.deleteOne(
        {"_id": new ObjectId(req.body._id)},
        function(err, results) {
          if (err) {
              console.log('Err with deleteOne - post - /delete: ' + err);
              res.json(err);
          } else {
              res.json(data);
          }
          db.close();
        }
      );
    });
  });

router
  .post('/update', function(req, res) {
    MongoClient.connect(url, function(err, db) {
      var collection = db.collection('books');

      collection.update(
        {"_id": new ObjectId(req.body._id._id)},
        {$set: req.body.changedBook},
        function(err, results) {
          if (err) {
              console.log('Err with update - post - /update: ' + err);
              res.json(err);
          } else {
              res.json(data);
          }
          db.close();
        });

    });
  });

router
  .get('/:page?', function(req, res) {
    var page = req.params.page;
    if(page) res.redirect('/');
  });

module.exports = router;