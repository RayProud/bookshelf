var express = require('express');
var routers = require('./routers/routers.js');
var path = require('path');
var bodyParser = require('body-parser');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/bookshelf';
var server = app.listen(1111, function() {
  console.log('Запустил сервер на локале 1111');
});

app.set('views', path.join(__dirname, './views/'));
app.set('view engine', 'jade');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/', routers);
app.use(express.static(path.join(__dirname, './public/')));