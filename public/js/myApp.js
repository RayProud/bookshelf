;(function(){
  var app = angular.module('booksApp', []);
  var books = [];
  app.controller('BooksController', ['$http', function($http) {
    this.bookList = books;
    var bookStore = this.bookList;

    this.addBook = function(book) {
      var that = this;

      $http
        .post('/books', this.book)
        .then(function(response) {
          // блокировать до саксесса
          this.bookList.push(response.data);
        },
        function(response) {
          console.log('Smth wrong with adding (post /books): ', response);
        });

        this.book = '';
    };

    this.removeBook = function(book) {

      var removingBook = {
        _id: book._id.toString(16)
      };

      $http
        .post('/delete', removingBook)
        .success(function(data, status, headers, config) {
            var index = bookStore.indexOf(book)
            bookStore.splice(index, 1);
          })
        .error(function(data, status, headers, config) {
            console.log('Smth wrong with deleting: ', data);
        });
    };

    var bool = true;

    this.validTest = function(bool) {
      this.bool = bool;
    };

    this.isValidTest = function(testBool) {
      return testBool === this.bool;
    };

    this.predicate = '';
    this.reverse = true;
    this.order = function(predicate) {
      this.reverse = (this.predicate === predicate) ? !this.reverse : false;
      this.predicate = predicate;
    };

    $http
      .get('/books')
      .success(function(data, status, headers, config) {
        this.bookList = books;
        this.bookList.push.apply(this.bookList, data);
    });
  }]);

  app.controller('EditBooksController', ['$http', '$scope', function($http, $scope) {
    $scope.editableBook = {};
    $scope.editable = false;

    $scope.editBook = function(bool, book) {
      $scope.editableBook = {
        _id: book._id,
        author: book.author,
        title: book.title
      };
      $scope.editable = bool;
    };

    $scope.update = function(bool, book, compareField, event) {
      $scope.editable = bool;

      if(event == 'blur' && !angular.equals($scope.editableBook, book)) {
        var _id = {
          _id: book._id.toString(16)
        };

        var changedBook = {};
        changedBook.author = book.author;
        changedBook.title = book.title;

        var response = {};
        response._id = _id;
        response.changedBook = changedBook;

        $http
          .post('/update', response)
          .error(function(data, status, headers, config) {
            console.log('Smth wrong with updating: ', data);
          });
      }
    };

  }]);

  app.directive('focusMe', function($timeout, $parse) {
    return {
      link: function(scope, element, attrs) {
        var model = $parse(attrs.focusMe);
        scope.$watch(model, function(value) {
          if(value === true) {
            $timeout(function() {
              element[0].focus();
            });
          }
        });

        element.bind('blur', function() {
           scope.$apply(model.assign(scope, false));
        });
      }
    };
  });

})();